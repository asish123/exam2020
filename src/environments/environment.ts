// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase : {
    apiKey: "AIzaSyB8xlNaZglYatd0hpbRQ8u_IJgU_m8LV2E",
    authDomain: "exam2020-a-1644f.firebaseapp.com",
    databaseURL: "https://exam2020-a-1644f.firebaseio.com",
    projectId: "exam2020-a-1644f",
    storageBucket: "exam2020-a-1644f.appspot.com",
    messagingSenderId: "651776551900",
    appId: "1:651776551900:web:470a6a080ed00b10a0bd19"
    }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
