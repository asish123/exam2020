import { PostsService } from './posts.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavComponent } from './nav/nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';

import { FormsModule }   from '@angular/forms';
import {MatCardModule} from '@angular/material/card';
import {MatInputModule} from '@angular/material';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatSelectModule} from '@angular/material';
import {MatExpansionModule} from '@angular/material/expansion';


import { AngularFireModule } from '@angular/fire';
import { environment } from '../environments/environment';
import { SignupComponent } from './signup/signup.component';
import { LoginComponent } from './login/login.component';
import { RouterModule, Routes } from '@angular/router';
import { AngularFireAnalyticsModule } from '@angular/fire/analytics';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import { HttpClientModule } from '@angular/common/http';
import { WelcomeComponent } from './welcome/welcome.component';
import { SavedpostComponent } from './savedpost/savedpost.component';
import { WelcomeuserComponent } from './welcomeuser/welcomeuser.component';
import { PostsComponent } from './posts/posts.component';


const appRoutes: Routes = [
  { path: 'signup', component: SignupComponent },
{ path: 'login', component: LoginComponent },
{ path: 'posts', component: PostsComponent },
{ path: 'posts/:id', component: PostsComponent },
{ path: 'savedpost', component: SavedpostComponent },
{ path: 'welcome', component: WelcomeComponent },
{ path: 'welcomeuser', component: WelcomeuserComponent },
  { path: '', redirectTo: '/', pathMatch: 'full'}
];

@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    SignupComponent,
    LoginComponent,
    WelcomeComponent,
    SavedpostComponent,
    WelcomeuserComponent,
    PostsComponent,
    
    
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    AngularFireModule.initializeApp(environment.firebase, 'exam2020'),
    RouterModule.forRoot(appRoutes,{ enableTracing: true }),
    AngularFireAnalyticsModule,
    AngularFirestoreModule,
    AngularFireStorageModule,
    AngularFireAuthModule,
    FormsModule,
    MatCardModule,
    MatInputModule,
    MatFormFieldModule,
    MatSelectModule,
    HttpClientModule,
    MatExpansionModule
  ],
  providers: [AngularFirestore,AngularFireAuth,PostsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
