import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Posts } from './interface/posts';
import { AngularFirestore } from '@angular/fire/firestore';
import { User } from './interface/user';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PostsService {
  apiurl = "http://jsonplaceholder.typicode.com/posts"
  apiurluser = "https://jsonplaceholder.typicode.com/users"
  apicomments ="http://jsonplaceholder.typicode.com/comments"

  constructor(private _http: HttpClient,private db:AngularFirestore) { }

  getPost(){
        return this._http.get<Posts[]>(this.apiurl);
      }

  addPosts(body:string, title:string,name:string){
                const post = {body:body, title:title, name:name};
                this.db.collection('posts').add(post);
              }

      getUser(){
       return this._http.get<User[]>(this.apiurluser);
      }


}