import { Component, OnInit } from '@angular/core';
import { Posts } from '../interface/posts';
import { PostsService } from '../posts.service';
import { User } from '../interface/user';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {

  panelOpenState = false;
  Posts$: Posts[];
  title:string;
  body:string;
  name:string;
  $User: User[];
  
  constructor(private postService:PostsService) { }

  ngOnInit() {
    this.postService.getPost()
   .subscribe(data =>this.Posts$ = data );
   this.postService.getUser().
   subscribe(data => this.$User = data);
  }
  postsFunction(){
    let postsArray = this.Posts$;
    let usersArray = this.$User;
    for(let i=0; i<postsArray.length; i++){
    for(let j=0; j<usersArray.length; j++){
        if (postsArray[i].userId == usersArray[j].id){
          this.postService.addPosts(postsArray[i].body,postsArray[i].title,usersArray[j].name)
        }
      }
    }
    alert("Saved for later viewing")
  }

}
