import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { PostsService } from '../posts.service';

@Component({
  selector: 'app-savedpost',
  templateUrl: './savedpost.component.html',
  styleUrls: ['./savedpost.component.css']
})
export class SavedpostComponent implements OnInit {
  likes = 0;
  panelOpenState = false;
  $fbPost: Observable<any[]>

  constructor(private postService: PostsService) { }

  ngOnInit() {
    this.$fbPost=this.postService.getPost();
  }
  addLikes(){
    this.likes++
  }

}
