import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { User } from 'firebase';
import { AngularFireAuth } from '@angular/fire/auth';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  user: Observable<User | null>;
  
  constructor(public afAuth:AngularFireAuth, private router:Router) { 
    this.user = this.afAuth.authState;
  }

  signup(email:string, passwoerd:string){
        this.afAuth.auth
            .createUserWithEmailAndPassword(email,passwoerd)
            .then(res => 
              {
              console.log('Succesful sign up',res);
              this.router.navigate(['/welcomeuser']);
                                      }  );       
            
      }
      Logout(){
         this.afAuth.auth.signOut()
        .then(res => console.log('Successful logout',res));
          }

    login(email:string, password:string){
         this.afAuth
          .auth.signInWithEmailAndPassword(email,password)
          .then(res =>  
                          {
                          console.log('Succesful Login',res);
                          this.router.navigate(['/welcomeuser']);
                          } 
                    )   
              }
}

