import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  public email:string;
  password:string;
  
  constructor(private atuh:AuthService,private router:Router) { 
  }
  ngOnInit() {
  }
  
  OnSubmitSignUp(){
    this.atuh.signup(this.email,this.password);
        this.router.navigate(['/welcomeuser']);
  }

}
